// [SECTION] Import
import { Fragment } from 'react';
import Slider from '../components/Slider';
import FeaturedProducts from './FeaturedProducts';
import ShopNowButton from '../components/ShopNowButton';
import Footer from '../components/Footer';

// [SECTION] Function
export default function ClientHome(){
	return (
		<Fragment>
			<Slider />
			<FeaturedProducts />
			<ShopNowButton />
			<Footer />
		</Fragment>
	)
}