// [SECTION] Imports
import { useState, useEffect, useContext } from 'react';
import { Form, Button, Row, Col } from 'react-bootstrap';
import { Navigate, useNavigate } from 'react-router-dom';

import Swal2 from 'sweetalert2';

import UserContext from '../UserContext';

// [SECTION] Functions
export default function ClientLogin() {
	const navigate = useNavigate();

	const [email, setEmail] = useState('');
	const [password, setPassword] = useState('');

	const [isActive, setIsActive] = useState(true);

	const { user, setUser } = useContext(UserContext);

	function authenticate(e) {
		e.preventDefault();

		fetch(`${process.env.REACT_APP_API_URL}/clients/login`, {
			method: 'POST',
			headers: {
				'Content-Type' : 'application/json'
			},
			body: JSON.stringify({
				email: email,
				password: password
			})
		})
			.then(response => response.json())
			.then(data => {
				console.log(data);
				if (data === false) {
					Swal2.fire({
						title: "Login Unsuccessful!",
						icon: "error",
						text: "Check you login credentials and try again"
					})
				} else {
					localStorage.setItem('token', data.access);
					retrieveClientDetails(data.access);

					Swal2.fire({
					    title: "Login successful!",
					    icon: "success",
					    text: "Welcome to Zuitt!"
					})

					navigate('/products');
				}
			})
	}

	const retrieveClientDetails = (token) => {
	  fetch(`${process.env.REACT_APP_API_URL}/clients/details`, {
	    headers: {
	      Authorization: `Bearer ${token}`,
	    },
	  })
	    .then((response) => {
	      if (response.ok) {
	        return response.json();
	      } else {
	        throw new Error('Error retrieving client details');
	      }
	    })
	    .then((data) => {
	      setUser({
	        id: data._id,
	        isAdmin: data.isAdmin,
	      });
	    })
	    .catch((error) => {
	      console.error('Error:', error);
	      // Handle the error condition appropriately, e.g., show an error message
	    });
	};



	useEffect(() => {
		if(email !== '' && password !== ''){
			setIsActive(false);
		} else {
			setIsActive(true);
		}
	}, [email, password]);

	return (
	    user.id === null || user.id ===undefined
	    ?
	        <Row>
	            <Col className = 'col-6 mx-auto'>
	                <h1 className = 'text-center mt-2'>Login</h1>
	                <Form onSubmit={(e) => authenticate(e)}>
	                    <Form.Group controlId="userEmail">
	                        <Form.Label>Email address</Form.Label>
	                        <Form.Control 
	                            type="email" 
	                            placeholder="Enter email"
	                            value={email}
	                            onChange={(e) => setEmail(e.target.value)}
	                            required
	                        />
	                    </Form.Group>

	                    <Form.Group controlId="password">
	                        <Form.Label>Password</Form.Label>
	                        <Form.Control 
	                            type="password" 
	                            placeholder="Password"
	                            value={password}
	                            onChange={(e) => setPassword(e.target.value)}
	                            required
	                        />
	                    </Form.Group>
	                    
	                    <Button variant="primary" type="submit" id="submitBtn" disabled = {isActive} className ='mt-3'>
	                            Login
	                        </Button>
	                    
	                </Form>
	            </Col>
	        </Row>

	    :
	      <Navigate to = '/*' /> 
	)
}
