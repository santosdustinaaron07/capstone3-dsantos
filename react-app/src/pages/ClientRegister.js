// [SECTION] Imports
import { Button, Form, Row, Col } from 'react-bootstrap';
import { useState, useEffect } from 'react';
import { useNavigate } from 'react-router-dom';

// import UserContext from '../UserContext';

import Swal2 from 'sweetalert2';

// [SECTION] Functions
export default function ClientRegister() {
	const [email, setEmail] = useState('');
	const [password, setPassword] = useState('');

	// const { user, setUser } = useContext(UserContext);

	const navigate = useNavigate();

	// const [isPassed, setIsPassed] = useState(true);

	// const [isAcctNamePassed, setAcctNameIsPassed] = useState(true);
	// const [isMobileNoPassed, setMobileNoIsPassed] = useState(true);
	// const [isAddressPassed, setAddressPassed] = useState(true);

	const [isDisabled, setIsDisabled] = useState(true);

	const [acctName, setAcctName] = useState('');
	const [mobileNo, setMobileNo] = useState('');
	const [address, setAddress] = useState('');

	useEffect(() => {
		if(acctName !== '' && email !== '' && password !== '' && address !== '') {
			setIsDisabled(false);
		} else {
			setIsDisabled(true);
		}
	}, [acctName, email, mobileNo, password, address]);

	function registerClient(event) {
		event.preventDefault();

		fetch(`${process.env.REACT_APP_API_URL}/clients/register`, {
			method: 'POST',
			headers: {
				'Content-Type' : 'application/json'
			},
			body: JSON.stringify({
				acctName: acctName,
				mobileNo: mobileNo,
				address: address,
				email: email,
				password: password
			})
		})
			.then(response => response.json())
			.then(data => {
				if(data === false){
					Swal2.fire({
						title: "Registration unsuccessful",
						icon: 'error',
						text: "Check the details provided and try again"
					})
				} else {
					navigate('/login');
				}
			})
	}

		return(
			// user.id === null || user.id === undefined
			// ?
			<Row>
				<Col className = "col-6 mx-auto">
					<h1 className = "text-center">Register</h1>
					<Form onSubmit ={event => registerClient(event)}>
							<Form.Group className="mb-3" controlId="formBasicAcctName">
							  <Form.Label>Account Name</Form.Label>
							  <Form.Control 
							  	type="acctName" 
							  	placeholder="Enter first name" 
							  	value = {acctName}
							  	onChange = {event => setAcctName(event.target.value)}
							  	/>
							</Form.Group>

							<Form.Group className="mb-3" controlId="formBasicMobileNo">
							  <Form.Label>Mobile Number</Form.Label>
							  <Form.Control 
							  	type="mobileNo" 
							  	placeholder="Enter mobile number" 
							  	value = {mobileNo}
							  	onChange = {event => setMobileNo(event.target.value)}
							  	/>
							</Form.Group>

					      <Form.Group className="mb-3" controlId="formBasicEmail">
					        <Form.Label>Email address</Form.Label>
					        <Form.Control 
					        	type="email" 
					        	placeholder="Enter email" 
					        	value = {email}
					        	onChange = {event => setEmail(event.target.value)}
					        	/>
					      </Form.Group>

					      <Form.Group className="mb-3" controlId="formBasicPassword">
					        <Form.Label>Password</Form.Label>
					        <Form.Control 
					        	type="password" 
					        	placeholder="Password" 
					        	value = {password}
					        	onChange = {event => setPassword(event.target.value)}
					        	/>
					      </Form.Group>

					      <Form.Group className="mb-3" controlId="formBasicAddress">
					        <Form.Label>Address</Form.Label>
					        <Form.Control 
					        	type="address" 
					        	placeholder="Address" 
					        	value = {address}
					        	onChange = {event => setAddress(event.target.value)}
					        	/>
					      </Form.Group>			      

					      <Button variant="primary" type="submit" disabled = {isDisabled}>
					        Sign up
					      </Button>
					</Form>
				</Col>
			</Row>
			// :
			// <Login/>
			)
}