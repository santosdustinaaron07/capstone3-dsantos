// [SECTION] Imports
import { Button, Form, Row, Col } from 'react-bootstrap';
import { useState, useEffect } from 'react';
import { useNavigate } from 'react-router-dom';

import Swal2 from 'sweetalert2';

// [SECTION] Functions
export default function AdminRegister() {
	const [email, setEmail] = useState('');
	const [password, setPassword] = useState('');
	const navigate = useNavigate();

	const [isDisabled, setIsDisabled] = useState(true);

	const [firstName, setFirstName] = useState('');
	const [lastName, setLastName] = useState('');
	const [mobileNo, setMobileNo] = useState('');

	useEffect(() => {
		if(firstName !== '' && lastName !== '' && email !== '' && password !== '') {
			setIsDisabled(false);
		} else {
			setIsDisabled(true);
		}
	}, [firstName, lastName, email, mobileNo, password]);

	function registerAdmin(event) {
		event.preventDefault();

		fetch(`${process.env.REACT_APP_API_URL}/admin/register`, {
			method: 'POST',
			headers: {
				'Content-Type' : 'application/json'
			},
			body: JSON.stringify({
				firstName: firstName,
				lastName: lastName,
				mobileNo: mobileNo,
				email: email,
				password: password
			})
		})
			.then(response => response.json())
			.then(data => {
				if(data === false){
					Swal2.fire({
						title: "Registration unsuccessful",
						icon: 'error',
						text: "Check the details provided and try again"
					})
				} else {
					navigate('/login');
				}
			})
	}

		return(
			// user.id === null || user.id === undefined
			// ?
			<Row>
				<Col className = "col-6 mx-auto">
					<h1 className = "text-center">Register</h1>
					<Form onSubmit ={event => registerAdmin(event)}>
							<Form.Group className="mb-3" controlId="formBasicFirstName">
							  <Form.Label>First Name</Form.Label>
							  <Form.Control 
							  	type="firstName" 
							  	placeholder="Enter first name" 
							  	value = {firstName}
							  	onChange = {event => setFirstName(event.target.value)}
							  	/>
							</Form.Group>

							<Form.Group className="mb-3" controlId="formBasicLastName">
							  <Form.Label>Last Name</Form.Label>
							  <Form.Control 
							  	type="lastName" 
							  	placeholder="Enter last name" 
							  	value = {lastName}
							  	onChange = {event => setLastName(event.target.value)}
							  	/>
							</Form.Group>

							<Form.Group className="mb-3" controlId="formBasicMobileNo">
							  <Form.Label>Mobile Number</Form.Label>
							  <Form.Control 
							  	type="mobileNo" 
							  	placeholder="Enter mobile number" 
							  	value = {mobileNo}
							  	onChange = {event => setMobileNo(event.target.value)}
							  	/>
							</Form.Group>

					      <Form.Group className="mb-3" controlId="formBasicEmail">
					        <Form.Label>Email address</Form.Label>
					        <Form.Control 
					        	type="email" 
					        	placeholder="Enter email" 
					        	value = {email}
					        	onChange = {event => setEmail(event.target.value)}
					        	/>
					      </Form.Group>

					      <Form.Group className="mb-3" controlId="formBasicPassword">
					        <Form.Label>Password</Form.Label>
					        <Form.Control 
					        	type="password" 
					        	placeholder="Password" 
					        	value = {password}
					        	onChange = {event => setPassword(event.target.value)}
					        	/>
					      </Form.Group>		      

					      <Button variant="primary" type="submit" disabled = {isDisabled}>
					        Sign up
					      </Button>
					</Form>
				</Col>
			</Row>
			// :
			// <Login/>
			)
}