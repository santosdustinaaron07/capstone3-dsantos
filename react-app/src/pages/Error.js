// [SECTION] Imports
import { Link } from 'react-router-dom';
import { Row, Col } from 'react-bootstrap'l

// [SECTION] Functions
export default function Error() {
	const pageStyle = {
		backgroundColor: '#18211B';
	}
	return (
		<div style={pageStyle}>
		  <Row>
		    <Col className="p-5">
		      <h1>Page Not Found</h1>
		      <p>Go back to the <Link to="/">homepage</Link>.</p>
		    </Col>
		  </Row>
		  <Row>
		    <Col className="p-5">
		      <img src={imageSrc} alt="Page Not Found" />
		    </Col>
		  </Row>
		</div>
	)
}