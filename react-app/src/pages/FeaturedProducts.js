// [SECTION] Import
import { Fragment, useState, useEffect } from 'react';
import { Row, Col } from 'react-bootstrap';

import FeaturedProductCard from '../components/FeaturedProductCard';

import './FeaturedProduct.css';

export default function FeaturedProduct() {
  const [featuredProducts, setFeaturedProducts] = useState([]);

  useEffect(() => {
    fetch(`${process.env.REACT_APP_API_URL}/products/sale`)
      .then(response => response.json())
      .then(data => {
        setFeaturedProducts(data.map(featuredProduct => {
          return (
            <Col key={featuredProduct._id} xs={12} md={6} lg={4} className="d-flex justify-content-center">
              <FeaturedProductCard featuredProductProp={featuredProduct} />
            </Col>
          );
        }));
      });
  }, []);

  return (
    <Fragment>
      <Row className="mt-5 text-center py-3" style={{ backgroundColor: '#044469' }}>
        <h1 style={{ color: '#FFB800' }} className='my-3'>ON SALE PRODUCTS!</h1>
      </Row>
      <Row className="justify-content-center" style={{ backgroundColor: '#FFB800' }}>
        {featuredProducts}
      </Row>
    </Fragment>
  );
}
