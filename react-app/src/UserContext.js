// [SECTION] Imports
import React from 'react';

// [SECTION] Declarations
const UserContext = React.createContext();

export const UserProvider = UserContext.Provider;

export default UserContext;