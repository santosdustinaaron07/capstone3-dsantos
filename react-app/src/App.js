// [SECTION] Imports
import './App.css';

import { Container } from 'react-bootstrap';

import ClientHome from './pages/ClientHome';
import ClientLogin from './pages/ClientLogin';
import AdminLogin from './pages/AdminLogin';
import ClientRegister from './pages/ClientRegister';
import AdminRegister from './pages/AdminRegister';

import { useState, useEffect} from 'react';
import { UserProvider} from './UserContext';
import { BrowserRouter, Route, Routes } from 'react-router-dom';

function App() {
  // For clients
  const [ user, setUser ] = useState({
    id: null,
    isAdmin: null
  });

  const unsetUser = () => {
    localStorage.clear()
  };

  useEffect(() => {
    fetch(`${process.env.REACT_APP_API_URL}/clients/details`, {
      headers: {
        Authorization: `Bearer ${localStorage.getItem('token')}`
      }
    })
      .then(response => response.json())
      .then(data => {
        setUser({
          id: data._id,
          isAdmin: data.isAdmin
        })
      })
  }, [])

  // For admins
  const [ adminUser, setAdminUser ] = useState({
    id: null,
    isAdmin: null
  });

  const unsetAdminUser = () => {
    localStorage.clear()
  };

  useEffect(() => {
    fetch(`${process.env.REACT_APP_API_URL}/admin/details`, {
      headers: {
        Authorization: `Bearer ${localStorage.getItem('token')}`,
      },
    })
      .then((response) => response.json())
      .then((data) => {
        setAdminUser({
          id: data._id,
          isAdmin: data.isAdmin,
        });
      })
      .catch((error) => {
        console.error('Error:', error);
        // Handle the error condition appropriately, e.g., show an error message
      });
  }, []);

  return (
    <UserProvider value = {{ user, setUser, unsetUser, adminUser, setAdminUser, unsetAdminUser }}>
      <BrowserRouter>
        <Container>
          <Routes>
            <Route path = '/' element = {<ClientHome />} />
            <Route path = '/login' element = {<ClientLogin />} />
            <Route path = '/admin' element = {<AdminLogin />} />
            <Route path = '/register' element = {<ClientRegister/>} />
            <Route path = '/admin/register' element = {<AdminRegister/>} />
          </Routes>
        </Container>
      </BrowserRouter>
    </UserProvider>
  );
}

export default App;
