// [SECTION] Imports
import { Row, Button, Col } from 'react-bootstrap';

import './ShopNowButton.css';

// [SECTION] Function
export default function ShopNowButton() {
  return (
    <Row>
      <Col xs={12} sm={6} className = "py-3">
        <div className="button-container py-sm-3">
          <Button size="lg" block className="custom-button px-5">
            Shop Now!
          </Button>
        </div>
      </Col>
      <Col xs={12} sm={6} className = "py-3">
        <div className="button-container py-sm-3">
          <Button size="lg" block className="custom-button px-5">
            Register Now!
          </Button>
        </div>
      </Col>
    </Row>
  );
}